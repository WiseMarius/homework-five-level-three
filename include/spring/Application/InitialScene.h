#pragma once
#include <spring\Framework\IScene.h>

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

namespace Spring
{
	class InitialScene : public IScene
	{
		Q_OBJECT

	public:

		explicit InitialScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~InitialScene();

	private:
		QWidget *centralWidget;
		QHBoxLayout *horizontalLayout;
		QGridLayout *gridLayout;
		QLabel *sceneLabel;
		QSpacerItem *verticalSpacer_2;
		QSpacerItem *horizontalSpacer_2;
		QSpacerItem *verticalSpacer;
		QSpacerItem *horizontalSpacer;
		QPushButton *nextPushButton;
		QPushButton *previousPushButton;
		QMenuBar *menuBar;
		QStatusBar *statusBar;

	private:
		void createGUI();

		private slots:
		void mf_NextButton();
	};

}
