#include "..\..\..\include\spring\Application\ApplicationModel.h"
#include <spring\Application\InitialScene.h>
#include <spring\Application\BaseScene.h>

const std::string initialSceneName = "Scene 1";
const std::string baseSceneName = "BaseScene";
const std::string mySceneName = "Scene ";

namespace Spring
{
	ApplicationModel::ApplicationModel()
	{
	}

	void ApplicationModel::defineScene()
	{
		IScene* initialScene = new InitialScene(initialSceneName);
		m_Scenes.emplace(initialSceneName, initialScene);
	
		std::string sceneName = "";
		for (unsigned short int sceneNumber = 1; sceneNumber < 1000; sceneNumber++)
		{
			sceneName = mySceneName + std::to_string(sceneNumber + 1);
			IScene* myScene = new BaseScene(sceneName);
			m_Scenes.emplace(sceneName, myScene);
		}
	}

	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = initialSceneName;
	}

	void ApplicationModel::defineTransientData()
	{
		//add initial values for all transient data 

	}
}
