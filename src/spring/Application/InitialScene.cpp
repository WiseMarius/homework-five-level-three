#include "..\..\..\include\spring\Application\InitialScene.h"
#include <qapplication.h>
#include <iostream>

namespace Spring
{
	InitialScene::InitialScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
		createGUI();

		QObject::connect(nextPushButton, SIGNAL(released()), this, SLOT(mf_NextButton()));
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{
	}

	void InitialScene::createGUI()
	{
		m_uMainWindow->resize(200, 100);

		centralWidget = new QWidget(m_uMainWindow.get());

		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		horizontalLayout = new QHBoxLayout(centralWidget);
		horizontalLayout->setSpacing(6);
		horizontalLayout->setContentsMargins(11, 11, 11, 11);
		horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(6);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		sceneLabel = new QLabel(centralWidget);
		sceneLabel->setObjectName(QStringLiteral("sceneLabel"));

		gridLayout->addWidget(sceneLabel, 1, 1, 1, 1, Qt::AlignHCenter | Qt::AlignVCenter);

		verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer_2, 0, 1, 1, 1);

		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer_2, 1, 2, 1, 1);

		verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer, 3, 1, 1, 1);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer, 1, 0, 1, 1);

		nextPushButton = new QPushButton(centralWidget);
		nextPushButton->setObjectName(QStringLiteral("nextPushButton"));

		gridLayout->addWidget(nextPushButton, 2, 2, 1, 1);

		previousPushButton = new QPushButton(centralWidget);
		previousPushButton->setObjectName(QStringLiteral("previousPushButton"));
		previousPushButton->setDisabled(true);

		gridLayout->addWidget(previousPushButton, 2, 0, 1, 1);


		horizontalLayout->addLayout(gridLayout);

		m_uMainWindow.get()->setCentralWidget(centralWidget);
		menuBar = new QMenuBar(m_uMainWindow.get());
		menuBar->setObjectName(QStringLiteral("menuBar"));
		menuBar->setGeometry(QRect(0, 0, 426, 26));
		m_uMainWindow.get()->setMenuBar(menuBar);
		statusBar = new QStatusBar(m_uMainWindow.get());
		statusBar->setObjectName(QStringLiteral("statusBar"));
		m_uMainWindow.get()->setStatusBar(statusBar);

		m_uMainWindow.get()->setWindowTitle(QApplication::translate("MainWindow", "My Scenes", Q_NULLPTR));
		sceneLabel->setText(QApplication::translate("MainWindow", "Scene 1", Q_NULLPTR));
		nextPushButton->setText(QApplication::translate("MainWindow", "Next", Q_NULLPTR));
		previousPushButton->setText(QApplication::translate("MainWindow", "Previous", Q_NULLPTR));
	}

	void InitialScene::mf_NextButton()
	{
		std::string nextSceneName = std::string("Scene " + std::to_string(2));

		emit SceneChange(nextSceneName);
	}
}

