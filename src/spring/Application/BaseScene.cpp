#include "..\..\..\include\spring\Application\BaseScene.h"

namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void BaseScene::createScene()
	{
		createGUI();

		if (currentSceneNumber < 1000)
		{
			QObject::connect(nextPushButton, SIGNAL(released()), this, SLOT(mf_NextButton()));
		}

		QObject::connect(previousPushButton, SIGNAL(released()), this, SLOT(mf_PreviousButton()));
	}

	void BaseScene::createGUI()
	{
		std::string appName = sGetSceneName();

		m_uMainWindow->setWindowTitle(QString(appName.c_str()));

		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		horizontalLayout = new QHBoxLayout(centralWidget);
		horizontalLayout->setSpacing(6);
		horizontalLayout->setContentsMargins(11, 11, 11, 11);
		horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(6);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		sceneLabel = new QLabel(centralWidget);
		sceneLabel->setObjectName(QStringLiteral("sceneLabel"));

		gridLayout->addWidget(sceneLabel, 1, 1, 1, 1, Qt::AlignHCenter | Qt::AlignVCenter);

		verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer_2, 0, 1, 1, 1);

		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer_2, 1, 2, 1, 1);

		verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer, 3, 1, 1, 1);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer, 1, 0, 1, 1);

		nextPushButton = new QPushButton(centralWidget);
		nextPushButton->setObjectName(QStringLiteral("nextPushButton"));

		if (currentSceneNumber >= 1000)
		{
			nextPushButton->setEnabled(false);
		}

		gridLayout->addWidget(nextPushButton, 2, 2, 1, 1);

		previousPushButton = new QPushButton(centralWidget);
		previousPushButton->setObjectName(QStringLiteral("previousPushButton"));

		gridLayout->addWidget(previousPushButton, 2, 0, 1, 1);


		horizontalLayout->addLayout(gridLayout);

		m_uMainWindow.get()->setCentralWidget(centralWidget);
		menuBar = new QMenuBar(m_uMainWindow.get());
		menuBar->setObjectName(QStringLiteral("menuBar"));
		menuBar->setGeometry(QRect(0, 0, 426, 26));
		m_uMainWindow.get()->setMenuBar(menuBar);
		statusBar = new QStatusBar(m_uMainWindow.get());
		statusBar->setObjectName(QStringLiteral("statusBar"));
		m_uMainWindow.get()->setStatusBar(statusBar);

		sceneLabel->setText(QString::fromStdString(sGetSceneName()));
		nextPushButton->setText(QApplication::translate("MainWindow", "Next", Q_NULLPTR));
		previousPushButton->setText(QApplication::translate("MainWindow", "Previous", Q_NULLPTR));
	}

	void BaseScene::release()
	{
		delete centralWidget;
	}

	BaseScene::~BaseScene()
	{
	}

	void BaseScene::mf_PreviousButton()
	{
		std::string nextSceneName = std::string("Scene " + std::to_string(currentSceneNumber - 1));

		emit SceneChange(nextSceneName);
	}

	void BaseScene::mf_NextButton()
	{
		std::string nextSceneName = std::string("Scene " + std::to_string(currentSceneNumber + 1));

		emit SceneChange(nextSceneName);
	}
}
